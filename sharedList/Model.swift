//
//  Model.swift
//  sharedList
//
//  Created by Ryan G Couper on 16/07/2016.
//  Copyright © 2016 Ryan G Couper. All rights reserved.
//

import Foundation 
import CoreData
import UIKit
import MapKit

class Model {
    // Some variables for the execution of the program
    var username = "Guest"
    private var selectedList:List?
    private var selectedListItems:[Item]? { // Doesn't need to be private as its read only, but
        get {                               // it is just so interfacing is the same accross the board
            return selectedList?.hasItems?.allObjects as? [Item]
        }
    }
    private var selectedItem:Item?
    
    
    // Add four variables for favourite lists ------------------------------------------------
    
/*
 * ITEMS
 * This section is for interacting with items
 */
    // Returns an item at a specified index path of a table view cell.
    func getItem(list:List, indexPath:NSIndexPath) -> Item {
        return selectedListItems![indexPath.row]
    }
    
    func getSelectedItem() -> Item? {
        return selectedItem
    }
    
    func itemIsSet() -> Bool {
        if let _ = selectedItem {
            return true
        }
        return false
    }
    
    func setSelectedItem(indexOfItemInSelectedListItems ind: Int) {
        selectedItem = selectedListItems![ind]
    }
    
    // This function creates a new item
    // creator is the username of the currently logged in person creating the list.
    func createItem(title ttl:String) {
        
        let item = createNewItemInDB()
        item.title = ttl
        item.quantity = 0
        item.createdBy = username
        item.dateCreated = NSDate()
        item.setValue(item.createdBy, forKey: "createdBy")
        item.setValue(item.dateCreated, forKey: "dateCreated")
        addItemToList(selectedList!, item: item)
        item.save()
    }
    
    // If you are creating an item, pass in nil as the existingItem parameter
    @available(*, deprecated=1.0, message="Use saveItem(item:Item, updater:String) instead")
    func saveItem(list:List, title ttl:String, address add:String, notes nts:String, quantity qty:NSNumber?, existingItem: Item?) {
        
        // Update details sequence
        func updateDetails(item:Item) {
            item.title = ttl
            item.address = add
            item.notes = nts
            item.quantity = qty
        }
        
        var item:Item
        if existingItem == nil {
            item = createNewItemInDB()
            addItemToList(list, item: item)
        } else {
            item = existingItem!
            updateItemInDB(selectedList!, title: ttl, address: add, notes: nts, quantity: qty, item: item)
        }
        
        updateDetails(item)
        updateDatabase()
    }
    
    func deleteItem(indexOfItemInSelectedListItems ind:Int) {
        deleteItemInDB(indexItemToDeleteInSelectedListItems: ind)
    }
    
/*
 * LISTS
 * This next section is for interaction with lists
 */
    // MARK: - CRUD for Lists
    
    //////////////////// Model Getter and Setter functions for a list ////////////////////
    
    // Use this to set the selectedList and selectedListItems
    func setSelectedList(list:List) {
        changeList(list)
    }
    
    // Returns the number of items in the currently selected list
    // Returns nil if the list has not recieved its first item.
    func getNumItemsInSelectedList() -> Int? {
        return selectedList?.hasItems?.count
    }
    
    func getNumItemsInList(list:List) -> Int? {
        return list.hasItems?.count
    }
    
    // Returns the immutable item array of the selected list
    func getSelectedListItems() -> [Item]? {
        return selectedListItems
    }
    
    // Returns the number of lists loaded
    func getNumOfLists() -> Int {
        return lists.count
    }
    
    // Just check if the list has been set
    func listIsSet() -> Bool {
        if let _ = selectedList {
            return true
        }
        return false
    }
    
    // Returns the currently selected lest
    func getSelectedList() -> List? {
        return selectedList
    }
    
    // Returns a list at the specified index's row.
    func getList(indexPath:NSIndexPath) -> List {
        return lists[indexPath.row] as! List
    }
    
    // Returns a list at the specified index
    func getList(index:Int) -> List {
        return lists[index] as! List
    }
    
    // Use this to create or update a list
    // For naming convenience...
    func createList(title ttl:String) {
        let list = createNewListInDB()
        list.name = ttl
        list.createdBy = username
        list.setValue(list.createdBy, forKey: "createdBy")
        list.dateCreated = NSDate()
        list.setValue(list.dateCreated, forKey: "dateCreated")
        list.save()
    }
    
    // If you are creating a list, pass in nil as the existingList parameter
    @available(*, deprecated=1.0, message="Use saveList(list:List2 instead.")
    func saveList(title ttl:String, existingList:List?) {
        
        
        let list:List
        if existingList != nil {
            list = existingList!
        } else {
            list = createNewListInDB()
        }
        let items:[Item] = list.hasItems?.allObjects as! [Item]
        
        updateListInDB(list, items: items, title: ttl)
        updateDatabase()
    }
    
    func deleteList(list:List) {
        deleteListInDB(list)
        loadData()
    }
    
    //////////////////////////////////////////////////////
    //      List functions for use in Model only        //
    //////////////////////////////////////////////////////
    
    // Use this to add an item to the list
    private func addItemToList(list:List, item:Item) {
        var items:[Item]
        items = list.hasItems?.allObjects as! [Item]
        items.append(item)
        saveItemsInList(list, items: items)
    }
    
    // internal to model class
    private func changeList(list:List) {
        selectedList = list
    }

/*
 * CORE DATA
 */
    // Create an array variable to hold the items stored in the database
    private var lists = [NSManagedObject]()
    //var items = [NSManagedObject]()
    
    // Short reference for the app delegate
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    // Create the managed context variable
    private var managedContext:NSManagedObjectContext
        {
        get {
            return appDelegate.managedObjectContext
        }
    }
    
    // MARK: - Database functionality
    // For loading the data from storage. 
    func loadData() {
        getListsFromDB()
    }
    
    /*
     * List Core Data functions
     */
    
    // This saves the current state of a list.
    // This function supercedes the updaetListInDB() function.
    // Pass the list in you wish to save, and if the array of items has been updated, pass in the array too, else
    // pass in nil as the second parameter.
    func saveList(list:List) {
        list.setValue(list.name, forKey: "name")
        list.setValue(username, forKey: "updateBy")
        list.setValue(NSDate(), forKey: "lastUpdated")
        list.setValue(list.administrator, forKey: "administrator")
        list.setValue(list.sharedWith, forKey: "sharedWith")
        updateDatabase()
        loadData()
    }
    
    private func saveItemsInList(list:List, items:[Item]) {
        let set = NSSet.init(array: items)
        list.setValue(set, forKey: "hasItems")
    }
    
    private func deleteListInDB(list:List) {
        managedContext.deleteObject(list)
        updateDatabase()
        if selectedList == list {
            selectedList = nil
        }
    }
    
    private func getListsFromDB() {
        do {
            let fetchRequest = NSFetchRequest(entityName: "List")
            
            let results = try managedContext.executeFetchRequest(fetchRequest)
            
            lists = results as! [List]
        } catch let error as NSError {
            print("failed to fetch items: \(error), \(error.userInfo)")
        }
    }
    
    private func createNewListInDB() -> List {
        let entity = NSEntityDescription.entityForName("List",
                                                       inManagedObjectContext: managedContext)
        return List(entity: entity!, insertIntoManagedObjectContext:managedContext)
    }
    
    /*
     * Item Core Data functions
     */
    
    // This function saves the state of the item. This function is to supercede
    // the updateItemInDB() function.
    func saveItem(item:Item) {
        item.setValue(item.address, forKey: "address")
        item.setValue(item.notes, forKey: "notes")
        item.setValue(item.quantity, forKey: "quantity")
        item.setValue(item.title, forKey: "title")
        item.setValue(item.expiryDate, forKey: "expiryDate")
        item.setValue(NSDate(), forKey: "lastUpdated")
        item.setValue(item.notesLastEdited, forKey: "notesLastEdited")
        item.setValue(item.notesEditedBy, forKey: "notesEditedBy")
        item.setValue(username, forKey: "updatedBy")
        updateDatabase()
        loadData()
    }
    
    func deleteItem(item:Item, indexOfItemInList ind:Int) {
        let list:List = item.belongsTo!
        var items:[Item] = list.hasItems!.allObjects as! [Item]
        items.removeAtIndex(ind)
        saveList(list)
    }
    
    private func createNewItemInDB() -> Item {
        // Get a variable that points to the entity in the data model
        let entity = NSEntityDescription.entityForName("Item", inManagedObjectContext: managedContext)
        return Item(entity: entity!, insertIntoManagedObjectContext:managedContext)
    }
    
    private func deleteItemInDB(indexItemToDeleteInSelectedListItems ind:Int) {
        var items:[Item] = getSelectedListItems()!
        items.removeAtIndex(ind)
        saveItemsInList(getSelectedList()!, items: items)
        updateDatabase()
        loadData()
    }
    
    func saveEverything() {
        if let _ = selectedList {
            saveList(selectedList!)
        }
        if let _ = selectedItem {
            saveItem(selectedItem!)
        }
    }
    
    private func updateDatabase() {
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not update database: \(error), \(error.userInfo)")
        }
    }
    
    // This function has been superceded by saveItem() - use that instead.
    @available(*, deprecated=1.0, message="Use saveItem(item:Item) instead.")
    private func updateItemInDB(list:List, title ttl:String, address add:String, notes nts:String, quantity qty:NSNumber?, item: Item) {
        item.setValue(ttl, forKey: "title")
        item.setValue(add, forKey: "address")
        item.setValue(nts, forKey: "notes")
        if let _ = qty {
            item.setValue(qty, forKey: "quantity")
        }
        var items:[Item]
        if let _ = list.hasItems {
            items = list.hasItems!.allObjects as! [Item]
            items.append(item)
        } else {
            items = [Item]()
            items.append(item)
        }
    }
    
    // This function has been superceded and is deprecated. Use saveList() instead.
    @available(*, deprecated=1.0, message="Use saveList(list:List) instead")
    private func updateListInDB(list:List, items:[Item]?, title:String) {
        list.name = title
        list.save()
        if let _ = items {
            saveItemsInList(list, items: items!)
        }
    }
    
/* 
 * STATIC INSTANCE
 * This next section is for creating the static instance of Model
 */
    // MARK: - Shared instance setup
    private init(){}
    private struct Static {
        static var instance:Model?
    }
    
    class var sharedInstance: Model {
        if(Static.instance == nil) {
            Static.instance = Model()
        }
        return Static.instance!
    }

}
