//
//  AuglistTableViewController.swift
//  sharedList
//
//  Created by Shane Drobnick on 19/08/2016.
//  Copyright © 2016 Ryan G Couper. All rights reserved.

/*  Based on Tutorials from
 
 Prashant  Mangukiya
 https://github.com/PrashantMangukiya/SwiftUIDemo 24 Oct 2015
 
 Mitchell Hudson
 https://www.youtube.com/watch?v=77EBBagKkLQ 26 Nov 2015
 
 */


import UIKit

class ListTableViewControllerV2: UITableViewController, UISearchBarDelegate
{
    let model:Model = Model.sharedInstance
    var list:List? {
        get {
            return model.getSelectedList()
        }
    }
    
    @IBOutlet weak var searchBar: UISearchBar!
    var isSearching : Bool = false
    var listSearch = [List]()
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        print("in search finc")
        if self.searchBar.text!.isEmpty {
            
            // set searching false
            self.isSearching = false
            
            // reload table view
            self.tableView.reloadData()
            
        } else {
            // set searching true
            self.isSearching = true
            
            // empty searching array
            self.listSearch.removeAll(keepCapacity: false)
            
            // find matching item and add it to the searching array
           for i in 0..<listSearch.count
           {
                let listItem = listSearch[i]
                if listItem.name!.lowercaseString.rangeOfString(self.searchBar.text!.lowercaseString) != nil
                {
                    self.listSearch.append(listItem)
                }
            }
            self.tableView.reloadData()
        }
    }
    
    //MARK: add to list button code here
    @IBOutlet weak var addlistButton: UIButton!
    @IBAction func addlistButtonAction(sender: UIButton)
    {
        if self.searchBar.text!.isEmpty
        {
            self.searchBar.resignFirstResponder()
            self.searchBar.endEditing(true)
        }
        else
        {
            // Add new list to the model.
            model.createList(title: self.searchBar.text!)
            
            self.searchBar.resignFirstResponder()
            self.searchBar.endEditing(true)
            self.isSearching = false
            self.searchBar.text = nil
            self.tableView.reloadData()
        }
    }
    
    private var collapseDetailViewController = true
    
    // MARK: - Prepare data for display
    // Make sure the latest data is displayed
    override func viewDidAppear(animated: Bool) {
        self.tableView.reloadData()
        
    }
    
    // Before updating the display, ensure you have the latest list of lists loaded.
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        model.loadData()
        self.title = "Your lists"
    }
    
    // MARK: - Populate the table
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.getNumOfLists()
    }
    
    // Create a cell with the Detailview accessory
    override func tableView(tableView:UITableView, cellForRowAtIndexPath indexPath:NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("listCell") as! ListTableViewCell
        
        cell.layer.cornerRadius = 10
        cell.layer.borderColor = UIColor.blackColor().CGColor
        cell.layer.borderWidth = 1
        
        let list = model.getList(indexPath)
        cell.itemNameLabel.text = list.name!
        cell.qtyLabel.text = "\(list.hasItems!.count)"
        
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        return cell
    }
    
    
    // MARTS - UITableViewDelegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        collapseDetailViewController = false
    }
    
    // MARK: - UISplitViewControllerDelegate
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController: UIViewController, ontoPrimaryViewController primaryViewController: UIViewController) -> Bool {
        return collapseDetailViewController
    }
    
    // MARK: - enable swipe to delete
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath:NSIndexPath) -> Bool {
        return true
    }
    
    // Override the system method that gets called when an item is deleted. This ensures its deleted from the managedContext too.
    override func tableView(tableView:UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath:NSIndexPath) {
        // Delete from managed context
        model.deleteList(model.getList(indexPath))
        
        // Delete from display
        self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
    }
    
    // MARK: - Prepare for segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        
        if segue.identifier == "listSelected" {
            let indexPath = tableView.indexPathForSelectedRow
            
            model.setSelectedList(model.getList(indexPath!))        }
    }
}