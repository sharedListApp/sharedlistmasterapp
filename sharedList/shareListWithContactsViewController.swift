//
//  shareListWithContactsViewController.swift
//  sharedList
//
//  Created by Ryan G Couper on 20/07/2016.
//  Copyright © 2016 Ryan G Couper. All rights reserved.
//

import UIKit

class shareListWithContactsViewController: UITableViewController {
    // Hard coded data for testing:
    var contacts:[String] = ["John Doe", "Nicolas Slater", "Shane Drobnick", "Ryan Couper", "Pretty girl at the shop", "Guy with weird teeth (Work)", "Jaquie Kelly", "Superman"]
    
    override func viewWillAppear(animated: Bool) {
        self.title = "Share with who?"
    }
    
    
    // Populate table
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("contactsCells", forIndexPath: indexPath)
            cell.textLabel!.text = contacts[indexPath.row]
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //let indexPath = tableView.indexPathForSelectedRow!
        
        let selectedCell = tableView.cellForRowAtIndexPath(indexPath)! as UITableViewCell
        
        if selectedCell.accessoryType == UITableViewCellAccessoryType.Checkmark {
            selectedCell.accessoryType = UITableViewCellAccessoryType.None
        } else {
            selectedCell.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
    }
}
