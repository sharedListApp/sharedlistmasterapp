//
//  ItemTableViewCell.swift
//  sharedList
//
//  Created by Shane Drobnick on 19/08/2016.
//  Copyright © 2016 Ryan G Couper. All rights reserved.
//

/*  Based on Tutorials from
 
 Prashant  Mangukiya
 https://github.com/PrashantMangukiya/SwiftUIDemo 24 Oct 2015
 
 Mitchell Hudson
 https://www.youtube.com/watch?v=77EBBagKkLQ 26 Nov 2015
 
 */

import UIKit

class ListTableViewCell: UITableViewCell
{
    let model:Model = Model.sharedInstance
    var list:List? {
        get {
            return model.getSelectedList()
        }
    }
    //MARK: IBoutlets
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var itemNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}