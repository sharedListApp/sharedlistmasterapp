//
//  Item+CoreDataProperties.swift
//  sharedList
//
//  Created by Ryan G Couper on 20/08/2016.
//  Copyright © 2016 Ryan G Couper. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Item {

    @NSManaged var address: String?
    @NSManaged var dateCreated: NSDate?
    @NSManaged var expiryDate: NSDate?
    @NSManaged var lastUpdated: NSDate?
    @NSManaged var notesEditedBy: String?
    @NSManaged var notes: String?
    @NSManaged var notesLastEdited: NSDate?
    @NSManaged var quantity: NSNumber?
    @NSManaged var title: String?
    @NSManaged var updatedBy: String?
    @NSManaged var createdBy: String?
    @NSManaged var belongsTo: List?

}
