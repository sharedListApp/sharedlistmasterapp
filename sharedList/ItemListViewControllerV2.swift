//
//  AugItemListViewController.swift
//  sharedList
//
//  Created by Shane Drobnick on 19/08/2016.
//  Copyright © 2016 Ryan G Couper. All rights reserved.

/*  Based on Tutorials from
 
 Prashant  Mangukiya
 https://github.com/PrashantMangukiya/SwiftUIDemo 24 Oct 2015
 
 Mitchell Hudson
 https://www.youtube.com/watch?v=77EBBagKkLQ 26 Nov 2015
 
 */


import UIKit

class ItemListViewControllerV2: UITableViewController, UISearchBarDelegate
{
    let model:Model = Model.sharedInstance
    var item:Item? {
        get {
            return model.getSelectedItem()
        }
    }
    // change to model  array is the normal list, searching is a temp array
    var array:[Item]? {
        get {
            return model.getSelectedListItems()
        }
    }
    //how you do the search with model?
    var arraySearching:[Item] = [Item]()
    
    var isSearching : Bool = false
    
    
    // 1@IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBar: UISearchBar!
    // 1 @IBOutlet weak var tableView: UITableView!
    
    
    //1@IBOutlet weak var addItemButton: UIButton!
    @IBOutlet weak var addItemButton: UIBarButtonItem!
    
    //1@IBAction func addItemButtonAction(sender: UIButton)
    @IBAction func addItemButtonAction(sender: UIBarButtonItem)
    {
        if self.searchBar.text!.isEmpty
        {
            self.searchBar.resignFirstResponder()
            self.searchBar.endEditing(true)
        }
        else
        {
            // Model somehow connect this
            model.createItem(title: self.searchBar.text!)
            arraySearching.removeAll(keepCapacity: true)
            
            self.searchBar.resignFirstResponder()
            self.searchBar.endEditing(true)
            self.isSearching = false
            self.searchBar.text = nil
            self.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        
        if model.listIsSet() {
            self.title = model.getSelectedList()?.name
            
        } else {
            self.title = "No List Selected"
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.searchBar.delegate = self
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.isSearching == true
        {
            return self.arraySearching.count
        }
        else
        {
            if let _ = array {
                return array!.count
            }
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("itemCell") as! ItemTableViewCell
        
        //this is making cells all fancy like brah!
//        cell.layer.cornerRadius = 10
//        cell.layer.borderColor = UIColor.blackColor().CGColor
//        cell.layer.borderWidth = 1
// Sorry Shane, these cell stylings look like a 90's webpage that lost its style sheets.
        
        
        //this needs to be changed to model
        
        func setCellIcons(item:Item, cell:ItemTableViewCell) {
            print("In setCellItcons")
            if (item.address == nil || item.address == "") {
                    cell.mapIcon.hidden = true
            } else {
                cell.mapIcon.hidden = false
            }
            
            if (item.notes == nil || item.notes == "") {
                cell.noteIcon.hidden = true
            } else {
                cell.noteIcon.hidden = false
            }
        }
        
        let item:Item
        if self.isSearching == true {
            item = arraySearching[indexPath.row]
            
        } else {
            item = array![indexPath.row]
        }
        
        cell.itemNameLabel.text = item.title
        cell.qtyLabel.text = "\(item.quantity!)"
//        cell.qtyStepper.value = Double(item.quantity!)
        
        setCellIcons(item, cell: cell)
        
        //if i want to add something to tapping accesstype
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        return cell
    }
    
    //MARK: Search bar delegate
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        
        if self.searchBar.text!.isEmpty {
            
            // set searching false
            self.isSearching = false
            
            // reload table view
            self.tableView.reloadData()
            
        } else {
            // set searching true
            self.isSearching = true
            
            // empty searching array
            self.arraySearching.removeAll(keepCapacity: false)
            
            // find matching item and add it to the searching array
            if let _ = array {
                let list = array!
                for i in 0..<list.count
                {
                    let listItem = list[i]
                    if listItem.title!.lowercaseString.rangeOfString(self.searchBar.text!.lowercaseString) != nil
                    {
                        self.arraySearching.append(listItem)
                    }
                }
            }
            self.tableView.reloadData()
        }
    }
    
    // This will delete a row with left swipe and confirm
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if editingStyle == UITableViewCellEditingStyle.Delete
        {
            //this needs to be changed for model
            model.deleteItem(array![indexPath.row], indexOfItemInList: indexPath.row)

            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }
    
    // hide keyboard when search button clicked
    func searchBarSearchButtonClicked(searchBar: UISearchBar)
    {
        
        self.searchBar.endEditing(true)
        self.searchBar.resignFirstResponder()
    }
    
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showItemDetailsSegue" {
            let indexPath = tableView.indexPathForSelectedRow
            model.setSelectedItem(indexOfItemInSelectedListItems: indexPath!.row)
        }
     }
 
}