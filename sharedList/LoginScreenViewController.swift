//
//  LoginScreenViewController.swift
//  sharedList
//
//  Created by Nicolas Slater on 19/07/2016.
//  Copyright © 2016 Ryan G Couper. All rights reserved.
//

import UIKit

class LoginScreenViewController: UIViewController {
    
    // username text input var
    @IBOutlet weak var usernameInput: UITextField!
    
    // password text input var
    @IBOutlet weak var passwordInput: UITextField!
    
    // login button var
    @IBOutlet weak var loginButtonOutlet: UIButton!
    @IBAction func loginButton(sender: AnyObject) {
        shouldPerformSegueWithIdentifier("loginSuccessSegue", sender: sender)
    }
    
    // once login button is pressed, check if username and password are correct
    func loginVerification(password password:String, username:String) -> Bool {
        if password == "password" && username == "username" {
            return true
        }
            
        else {
            return false
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
     
        let loginSuccess = loginVerification(password: passwordInput.text!, username: usernameInput.text!)
        if loginSuccess {
            return true
        } else {
            return false
        }
    }
}

