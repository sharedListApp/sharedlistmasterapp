//
//  PlaceholderAddListViewController.swift
//  sharedList
//
//  Created by Ryan G Couper on 17/07/2016.
//  Copyright © 2016 Ryan G Couper. All rights reserved.
//

import UIKit

class PlaceholderAddListViewController: UIViewController {
    
    var model:Model = Model.sharedInstance
    
    @IBOutlet weak var newListName: UITextField!
    
    @IBAction func saveNewList(sender: AnyObject) {
        if newListName.text == "" {
            newListName.text = "Untitled"
        }
        model.createList(title: newListName.text!)
        //model.saveList(title: newListName.text!, existingList: nil)
        
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    @IBAction func cancelNewList(sender: AnyObject) {
        self.navigationController?.popToRootViewControllerAnimated(true)
        
    }
}
