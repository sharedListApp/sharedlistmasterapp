//
//  ItemDetailViewController.swift
//  sharedList
//
//  Created by Ryan G Couper on 17/07/2016.
//  Copyright © 2016 Ryan G Couper. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ItemDetailViewController: UIViewController, UITextFieldDelegate, MKMapViewDelegate, CLLocationManagerDelegate {
    var model:Model = Model.sharedInstance
    var selItem:Item? {
        get {
            return model.getSelectedItem()
        }
    }
    
    // used for testing, when set to false, no test print statements will be shown
    var debug = false
    
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var QtyTextField: UITextField!
    
    @IBAction func beginEditingNotes(sender: AnyObject) {
        notesLabel.text! = "Hit done to submit"
    }
    
    @IBOutlet weak var notesTextField: UITextField!
    @IBAction func editNotesSubmitted(sender: AnyObject) {
        textFieldShouldReturn(notesTextField)
        notesLabel.text! = "Notes:"
//        model.saveItem(model.getSelectedList()!, title: selItem!.title!, address: selItem!.address!, notes: notesTextField.text!, quantity: selItem?.quantity!, existingItem: selItem!)
        selItem!.notes = notesTextField.text!
        selItem!.save()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @IBAction func newQty(sender: AnyObject) {
        if let newQuantity:NSNumber = NSNumber(integer: Int(QtyTextField.text!)!) {
            selItem!.quantity = newQuantity
            selItem!.save()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        notesTextField.delegate = self
        if let _ = selItem {
            self.title = selItem!.title
            if let _ = model.getSelectedList() {
                self.navigationItem.prompt = model.getSelectedList()!.name
            }
            QtyTextField.text = "\(selItem!.quantity!)"
            if selItem!.notes != "" {
                notesTextField.text = selItem!.notes
            }
        } else {
            if UIDevice.currentDevice().orientation == .Portrait {
                self.navigationItem.prompt = "Swipe in from left to see the full list"
            } else {
                
                self.navigationItem.prompt = "No List Selected"
            }
            self.title = "No Item Selected"
        }
    }
    
    //---------------------------------------------------------------------------------------------------------
    // Start GPS Code
    //---------------------------------------------------------------------------------------------------------

    // linking to the map view form the storyboard
    @IBOutlet weak var mapView: MKMapView! //done
    
    // address var
    var address:String {
        get {
            if let _ = addressInput.text {
                return addressInput.text!
            } else { return "" }
        }
    }
    
    // setting the location manager variable for future use
    let locationManager = CLLocationManager()
    
    // current location and destination class vars
    var currentLocation: CLLocationCoordinate2D = CLLocationCoordinate2D()
    var destination:MKMapItem = MKMapItem()
    
    // total distance class var
    var totalDist:Double = 0
    
    // current lat and long class var
    var currentLatitude:Double = 0
    var currentLongitude:Double = 0
    
    // outlet for text box input for map address
    @IBOutlet weak var addressInput: UITextField!
    
    
    // pin lat and long class vars
    var pinLat:Double = 0
    var pinLong:Double = 0
    
    // outlet for address not valid message
    @IBOutlet weak var addressMsg: UILabel!
    
    
    //  this function is triggered by the button that says 'open in maps'
    @IBAction func startFunc(sender: AnyObject) {
        openMaps(address)
    }
    
    // add destination function is called when the addressInput text box has had content entered
    @IBAction func addDestination(sender: AnyObject) {
        addDirection(address)
    }
    
    // overriding the view did load function
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (debug) {
            print("Success")
        }
        
        
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplicationWillResignActiveNotification, object: nil)
        
        // for hitting return
        addressInput.delegate = self
        
        // for tapping
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ItemDetailViewController.dismissKeyboard)))
        
        // conforming with delegate
        self.locationManager.delegate = self
        
        // setting the desired accuracy to best, because we want the exact location
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        // requesting authorization from the user with the use of a popup
        // the message can be changed in info.plist
        self.locationManager.requestAlwaysAuthorization()
        
        // starts looking for the devices location
        self.locationManager.startUpdatingLocation()
        
        // shows the blue dot on the map for the devices current location
        self.mapView.showsUserLocation = true;
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // keyboard hide - tapping anywhere
    func dismissKeyboard() {
        addressInput.resignFirstResponder()
    }
    
    // current location code
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // the location = the last lat and long found
        let location = locations.last
        
        // creating the coordinated using the lat and long given
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        
        // getting the current lat and long
        currentLatitude = center.latitude
        currentLongitude = center.longitude
        
        // test print statements
        if (debug) {
            print(currentLatitude)
            print(currentLongitude)
        }
        
        // print statement for testing
        if (debug) {
            print("current location is \(currentLocation)")
        }
        
        // setting the region for the map, the Delta is the zoom, the lower the number, the closer the zoom
        let region = MKCoordinateRegion(center: currentLocation, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
        
        // set the region to the region created above, and set animation for zoom effect to true
        self.mapView.setRegion(region, animated: true)
        
        // once all of this is complete, stop updating location
        self.locationManager.stopUpdatingLocation()
    }
    
    // catching any errors from the location manager
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        if (debug) {
            print("Errors" + error.localizedDescription)
        }
    }
    
    // outlet for showDirections button, this is necessary for the next funciton to work properly
    @IBOutlet weak var showDirections: UIButton!
    
    // brings up directions when the directions button is pressed
    @IBAction func showDirections(sender: AnyObject) {
        
        // Need to create a MKDiirection Request
        let request = MKDirectionsRequest()
        
        // request the destination
        request.source = MKMapItem.mapItemForCurrentLocation()
        request.destination = destination
        
        // dont give option for alterante routes
        request.requestsAlternateRoutes = false
        
        let directions = MKDirections(request: request)
        
        // get directions and show the path using MKOverlay
        directions.calculateDirectionsWithCompletionHandler({(response:
            MKDirectionsResponse?, error: NSError?) in
            
            if error != nil {
                
                if (self.debug) {
                    print("Error \(error)")
                }
                
            } else {
                
                self.mapView.delegate = self
                let overlays = self.mapView.overlays
                self.mapView.removeOverlays(overlays)
                
                for route in response!.routes {
                    
                    self.mapView.addOverlay(route.polyline,
                        level: MKOverlayLevel.AboveRoads)
                    
                    for next  in route.steps {
                        print(next.instructions)
                    }
                }
            }
        })
    }
    
    /*
     * drawing the rout on the map using map overlays
     * ensure that the delegate for the mapview in the storyboard
     * is set to this class
    */
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer
    {
        let draw = MKPolylineRenderer(overlay: overlay)
        draw.strokeColor = UIColor.purpleColor()
        draw.lineWidth = 5.0
        return draw
    }
    
    // add direction function by using the button in the bottom right corner
    func addDirection(address: String){
        
        // getting the placemark location from the address input textbox
        let geocoder: CLGeocoder = CLGeocoder()
        geocoder.geocodeAddressString(address, completionHandler: {(placemarks: [CLPlacemark]?, error: NSError?) -> Void in
            
            // if there is already a placemark on the map, remove it, then add the new one, and update the lat and long vars
            if (placemarks?.count > 0) {
                
                // removing any existing placemarks
                self.mapView.removeAnnotations(self.mapView.annotations)
                
                // getting the position for the placemark
                let topResult: CLPlacemark = (placemarks?[0])!
                let placemark: MKPlacemark = MKPlacemark(placemark: topResult)
                var region: MKCoordinateRegion = self.mapView.region
                
                // getting the lat and long from the placemark
                region.center.latitude = (placemark.location?.coordinate.latitude)!
                region.center.longitude = (placemark.location?.coordinate.longitude)!
                
                // setting span values
                region.span = MKCoordinateSpanMake(0.5, 0.5)
                
                // setting the region for the placemark
                self.mapView.setRegion(region, animated: true)
                self.mapView.addAnnotation(placemark)
                
                // set the destination var to the current placemark
                self.destination = MKMapItem(placemark: placemark)
                
                // setting the class level pin lat and long vars to the current destination lat and long
                self.pinLat = placemark.coordinate.latitude
                self.pinLong = placemark.coordinate.longitude
                
                // print statement for testing, only runs if debug is equal to true
                if (self.debug) {
                    print("pin lat is \(self.pinLat) pinLong is \(self.pinLong))")
                }
                
                // for use with distance calculation
                let destLong = self.rad2deg(self.pinLong)
                let destLat = self.rad2deg(self.pinLat)
                let currLat = self.rad2deg(self.currentLatitude)
                let currLong = self.rad2deg(self.currentLongitude)
                
                // total dist calculated here
                self.totalDist = self.distance(destLat, destLong, currLat, currLong, unit: "K")
                
                // print statements for testing
                if (self.debug) {
                    print("Total distance is:")
                    print(self.totalDist)
                }
            }
                
            // if no location is found with the input provided, inform the user via a label message
            else {
                
                // show this message if the location is not found on the maps
                self.addressMsg.text = "Location not found on maps."
            }
        })
    }
    
    // open maps function by using the 'open in maps' button
    func openMaps(address: String){
        
        // getting the placemark location from the address input textbox
        let geocoder: CLGeocoder = CLGeocoder()
        geocoder.geocodeAddressString(address, completionHandler: {(placemarks: [CLPlacemark]?, error: NSError?) -> Void in
            
            // if there is already a placemark on the map, remove it, then add the new one, and update the lat and long vars
            if (placemarks?.count > 0) {
                
                // remove any existing placemarks
                self.mapView.removeAnnotations(self.mapView.annotations)
                
                // setting the placemark to the top result var
                let topResult: CLPlacemark = (placemarks?[0])!
                let placemark: MKPlacemark = MKPlacemark(placemark: topResult)
                var region: MKCoordinateRegion = self.mapView.region
                
                // retrieving the lat and long to get the coordinates for the placemark
                region.center.latitude = (placemark.location?.coordinate.latitude)!
                region.center.longitude = (placemark.location?.coordinate.longitude)!
                
                // set the region span values
                region.span = MKCoordinateSpanMake(0.5, 0.5)
                
                // set the region for the mapView and add the placemark
                self.mapView.setRegion(region, animated: true)
                self.mapView.addAnnotation(placemark)
                
                // setting the destination variable to where the current placemark is
                self.destination = MKMapItem(placemark: placemark)
                
                // setting pin lat and long class vars to the new coordinates
                self.pinLat = placemark.coordinate.latitude
                self.pinLong = placemark.coordinate.longitude
                
                // print for testing
                if (self.debug) {
                    print("pin lat is \(self.pinLat) pinLong is \(self.pinLong))")
                }
                
                // for use with distance calculation
                let destLong = self.rad2deg(self.pinLong)
                let destLat = self.rad2deg(self.pinLat)
                let currLat = self.rad2deg(self.currentLatitude)
                let currLong = self.rad2deg(self.currentLongitude)
                
                // total dist calculated here
                self.totalDist = self.distance(destLat, destLong, currLat, currLong, unit: "K")
                
                // print statements for testing
                if (self.debug) {
                    print("Total distance is:")
                    print(self.totalDist)
                }
                
                //Create map item and open in map app
                let item = MKMapItem(placemark: placemark)
                item.name = address
                item.openInMapsWithLaunchOptions(nil)
                
            }
                
            // if no location is found with the input provided, inform the user via a label message
            else {
                
                // show this message if the location is not found on the maps
                self.addressMsg.text = "Location not found on maps."
            }
        })
    }
    
    // convert degrees to radials
    func deg2rad(deg:Double) -> Double {
        return deg * M_PI / 180
    }
    
    // convert radials to degrees
    func rad2deg(rad:Double) -> Double {
        return rad * 180.0 / M_PI
    }
    
    // getting the distance from the current location and the entered destincation
    func distance(lat1:Double, _ lon1:Double, _ lat2:Double, _ lon2:Double, unit:String) -> Double {
        
        // calculating the distance between the 2 different lat/long values
        let theta = lon1 - lon2
        var dist = sin(deg2rad(lat1)) * sin(deg2rad(lat2)) + cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * cos(deg2rad(theta))
        dist = acos(dist)
        
        // use 'K' in the function call for KMs
        dist = dist * 60 * 1.1515
        if (unit == "K") {
            dist = dist * 1.609344
        }
            
        // use 'N' for nautical miles
        else if (unit == "N") {
            dist = dist * 0.8684
        }
        
        // return the distance
        return dist
    }
    
    // sending the notification if the item location is within 10kms
    func sendNot(sentence :String, time:NSTimeInterval) {
        
        // set the local notification var to a UILocal notification object
        let localNotification = UILocalNotification()
        
        // setting the fire date, and getting the time passed since it was sent
        localNotification.fireDate = NSDate(timeIntervalSinceNow: time)
        
        // set the alert to say the sentence
        localNotification.alertBody = sentence
        
        // getting the local time
        localNotification.timeZone = NSTimeZone.defaultTimeZone()
        
        // setting the badge icons number for showing notifications
        localNotification.applicationIconBadgeNumber = UIApplication.sharedApplication().applicationIconBadgeNumber + 1
        
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    }
    
    // functions that will be executed when the app has moved to the background
    func appMovedToBackground() {
        
        if (debug) {
            print("App moved to background!")
        }
        
        if (totalDist == 0) {
            if (debug) {
                print("no address entered")
            }
        }
        
        // if the total distance is less than 2KMs away, send the notification
        if (totalDist < 2 && totalDist != 0) {
            
            // send the notification after 5 second once the app has entered background mode
            sendNot("You are near \(address), click here to get directions!", time: 5)
            
        }
    }
}
