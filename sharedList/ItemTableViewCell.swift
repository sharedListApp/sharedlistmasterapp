//
//  ItemTableViewCell.swift
//  sharedList
//
//  Created by Shane Drobnick on 19/08/2016.
//  Copyright © 2016 Ryan G Couper. All rights reserved.
//

/*  Based on Tutorials from
 
 Prashant  Mangukiya
 https://github.com/PrashantMangukiya/SwiftUIDemo 24 Oct 2015
 
 Mitchell Hudson
 https://www.youtube.com/watch?v=77EBBagKkLQ 26 Nov 2015
 
 */

import UIKit

class ItemTableViewCell: UITableViewCell
{
    let model:Model = Model.sharedInstance
    //MARK: IBoutlets
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var noteIcon: UIImageView!
    @IBOutlet weak var mapIcon: UIImageView!
//    @IBOutlet weak var qtyStepper: UIStepper!
    
    //MARK: IBActions
    //1@IBAction func qtyStepperAction(sender: UIStepper)
//    @IBAction func qtyStepperAction(sender: UIStepper)
//    {
//        //need to send this to model somehow, does not remember value since it is meant to be updating model
//        self.qtyLabel.text =  "\(Int(sender.value))"
//        
//        
//    }    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
    }
    
    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    
}
