//
//  secondFavTableViewController.swift
//  sharedList
//
//  Created by Shane Drobnick on 11/07/2016.
//  Copyright © 2016 Ryan G Couper. All rights reserved.
//

import UIKit

class secondFavTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableView: UITableView!
    
    //var names = ["Mark","Clyde","Peter","Benson","William","Zelda","Link","Mario","Luigi","Snake","Zubat","Commando", "AR15","Anger Managment for a Dev","HTC Vive"]
    var name = listObject.init()
    
    override func viewDidLoad() {
        
        name.addItem(itemObject: itemObject.init(title: "Zelda"))
        name.addItem(itemObject: itemObject.init(title: "Link"))
        name.addItem(itemObject: itemObject.init(title: "Peter"))
        name.addItem(itemObject: itemObject.init(title: "Dragon Eggs"))
        name.addItem(itemObject: itemObject.init(title: "Foo",
            address: "The Foo Shop", notes: "Get Master Foo"))
        name.addItem(itemObject: itemObject.init(title: "Train pictures"))
        name.addItem(itemObject: itemObject.init(title: "Yo Mamma Jokes"))
        name.addItem(itemObject: itemObject.init(title: "Bitch Tits"))
        name.addItem(itemObject: itemObject.init(title: "Anal plugs"))
        name.addItem(itemObject: itemObject.init(title: "Nicolas' bum virginity"))
        name.addItem(itemObject: itemObject.init(title: "From Shane"))
        name.addItem(itemObject: itemObject.init(title: "HTC VIVE"))
        name.addItem(itemObject: itemObject.init(title: "Anger managment course for a dev"))
        name.addItem(itemObject: itemObject.init(title: "AR15 if that fails"))
        name.addItem(itemObject: itemObject.init(title: "Mario"))
        name.addItem(itemObject: itemObject.init(title: "Luigi"))
        name.addItem(itemObject: itemObject.init(title: "R-Type"))
        name.addItem(itemObject: itemObject.init(title: "Space Invaders"))
        name.addItem(itemObject: itemObject.init(title: "Mrs Pac Man"))
        name.addItem(itemObject: itemObject.init(title: "Hogan"))
        name.addItem(itemObject: itemObject.init(title: "Phantom Menace was horrible"))
        
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return name.getNumOfItems()
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cell") as! CustomCell
        
        let item: itemObject = name.getNextItemInList(indexPath)
        
        cell.name.text = item.getTitle()
        
        return cell
        
    }
    
    // This will delete a row with swipe
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if editingStyle == UITableViewCellEditingStyle.Delete
        {
            //name.removeAtIndex(indexPath.row)
            name.deleteItemInList(indexPath.row)
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
        

    }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
