//
//  List+CoreDataProperties.swift
//  sharedList
//
//  Created by Ryan G Couper on 19/08/2016.
//  Copyright © 2016 Ryan G Couper. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension List {

    @NSManaged var name: String?
    @NSManaged var dateCreated: NSDate?
    @NSManaged var createdBy: String?
    @NSManaged var updateBy: String?
    @NSManaged var lastUpdated: NSDate?
    @NSManaged var administrator: String?
    @NSManaged var sharedWith: String?
    @NSManaged var hasItems: NSSet?

}
