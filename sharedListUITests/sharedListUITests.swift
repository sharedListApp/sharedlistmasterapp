//
//  sharedListUITests.swift
//  sharedListUITests
//
//  Created by Ryan G Couper on 25/06/2016.
//  Copyright © 2016 Ryan G Couper. All rights reserved.
//

import XCTest

class sharedListUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // this was tested on an iPhone 6 simulator
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let app = XCUIApplication()
        
        // asserting that login button says "Login"
        XCTAssert(app.buttons["Login"].exists)
        
        // asserting that shared lists title is correct
        XCTAssert(app.staticTexts["Shared Lists"].exists)
        
        // tapping the login button
        XCUIApplication().buttons["Login"].tap()
        
        
        // assert that the text in the navigation bar says the "No Item Selected"
        XCTAssert(app.navigationBars["No Item Selected"].exists)
        
        // traversing through the pages while creating list and item objects
        app.navigationBars["No Item Selected"].buttons["Lists"].tap()
        app.navigationBars["A List Of Lists"].buttons["add list@2x"].tap()
        
        let window = app.childrenMatchingType(.Window).elementBoundByIndex(0)
        let element = window.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).elementBoundByIndex(1).childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element
        element.childrenMatchingType(.TextField).element.tap()
        element.childrenMatchingType(.TextField).element
        
        
        app.navigationBars["Create A New List"].buttons["Save"].tap()
        
        // asserting that the created list "new" exists
        XCTAssert(app.staticTexts["new"].exists)
        
        let tablesQuery = app.tables
        let newStaticText = tablesQuery.staticTexts["new"]
        newStaticText.tap()
        
        let newNavigationBar = app.navigationBars["new"]
        newNavigationBar.buttons["plus math"].tap()
        
        // asserting that the created item "newItem" exists
        XCTAssert(app.staticTexts["newItem"].exists)
        
        let element2 = window.childrenMatchingType(.Other).elementBoundByIndex(1)
        element2.childrenMatchingType(.Other).elementBoundByIndex(2).childrenMatchingType(.Other).elementBoundByIndex(1).childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).elementBoundByIndex(1).childrenMatchingType(.Other).element.childrenMatchingType(.TextField).element.tap()
        app.textFields.containingType(.Button, identifier:"Clear text").element
        element2.buttons["plus math"].tap()
        
        let listsButton = newNavigationBar.buttons["Lists"]
        listsButton.tap()
        listsButton.tap()
        newStaticText.tap()
        tablesQuery.staticTexts["newItem"].tap()
        
        
    }
    
}
