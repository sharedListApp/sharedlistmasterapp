//
//  Item.swift
//  sharedList
//
//  Created by Ryan G Couper on 17/07/2016.
//  Copyright © 2016 Ryan G Couper. All rights reserved.
//

import Foundation
import CoreData

/* When changing variables in this class, be sure to run
 * the save function after changing the variable
 */
class Item: NSManagedObject {
    var model:Model = Model.sharedInstance
// Insert code here to add functionality to your managed object subclass
    func save() {
        model.saveItem(self)
    }
}
